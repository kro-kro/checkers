class Checker {
    constructor(id, color, cell) {
        this.id = id;
        this.color = color;
        this.cell = cell;
        this.isKing;
    }

    move(board, cellTo, isBeatNeeded, checker = this) {
        let beated;
        if (isBeatNeeded) {
            beated = this.beat(checker.cell, cellTo, board);
        }
        checker.cell.checker = undefined;
        checker.cell = cellTo;
        cellTo.checker = checker;
        return beated;
    }

    beat(prevCell, currentCell, board) {
        let cell = board.getCellsBetween(prevCell, currentCell);
        let temp = this.getFarest(cell);
        let beaten = temp.checker;
        let pos = temp = temp.pos;
        board.cells[board.letters.indexOf(pos[0])][pos[1]].checker = undefined;
        return beaten;
    }

    getFarest(between) {
        if (between.length === 1) {
            return between.pop();
        }
        for (let i = 0; i < between.length; i++) {
            if (between[i].checker) {
                return between[i];
            }
        }
    }

    becomeKing(game, checker = this) {
        checker.isKing = true;
    }
}