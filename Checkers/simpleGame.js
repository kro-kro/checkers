class SimpleGame {
	constructor(board) {
		this.board = board;
		this.player = new Player('#cccccc');
		this.computer = new Computer('#774654');
	}
	
	putCheckers () {
		for (let i = 0; i < 3; i++) {
			for (let j = 1 + i % 2; j <= this.board.n; j += 2) {
				let checker = new Checker('computer' + this.computer.checkers.length, this.computer.color, this.board.cells[i][j]);
				this.board.cells[i][j].checker = checker;
				this.computer.checkers.push(checker);
			}
		}
		let count = this.board.n - 1;
		for (let i = count; i > count - 3; i--) {
			for (let j = 1 + i % 2; j <= this.board.n; j += 2) {
				let checker = new Checker('player' + this.player.checkers.length, this.player.color, this.board.cells[i][j])
				this.board.cells[i][j].checker = checker;
				this.player.checkers.push(checker);
			}
		}
	}
	
	getWinner(game, ruler) {
		let user = ruler.getAllMoves(game.board, game.player).length;
		let computer = ruler.getAllMoves(game.board, game.computer).length;
		if (user === 0 && computer === 0) {
			return 'Draw';
		}
		if (user === 0 && computer !== 0) {
			return 'Computer wins.';
		}
		if (user !== 0 && computer === 0) {
			return 'Player wins.';
		}
		return 'Oups, something wrong ._.';
	}
	
}