class Game {
    constructor(game) {
        this.game = game;
        this.selectedChecker;
        this.beatNow = false;
        this.playerTurn = true;
        this.ruler = new Ruler();
    }

    drawBoard(board, gameBoard) {
        for (let i = 0; i < board.n; i++) {
            for (let j = 1; j <= board.n; j++) {
                let cell = document.createElement("div");
                cell.className += "boardCell";
                cell.setAttribute("id", board.cells[i][j].pos);
                cell.setAttribute("title", board.cells[i][j].pos);
                let checker = board.cells[i][j].checker;
                if (checker) {
                    let docChecker = document.createElement("div");
                    docChecker.setAttribute("id", checker.id);
                    docChecker.className = "checker";
                    cell.appendChild(docChecker).style.backgroundColor = checker.color;
                }
                gameBoard.appendChild(cell).style.backgroundColor = board.cells[i][j].color;
            }
        }
    }

    firstClick(cell) {
        let iBNresult = this.ruler.isBeatNeeded(this.game.board, this.game.player)
        let checkerBeats = iBNresult.map(ibn => ibn.checker)
        if (checkerBeats.length) {
            if (cell && checkerBeats.indexOf(cell.checker) !== -1) {
                this.beatNow = true;
                this.selectedChecker = cell.checker;
                if (this.selectedChecker) this.setSelected();
            }
        } else {
            let temp = this.ruler.getAllMoves(this.game.board, this.game.player).map((val) => {
                return val.split(' ')[0];
            });
            if (cell.checker && cell.checker.color !== this.game.player.color) {
                return;
            }
            this.selectedChecker = cell.checker;
            if (this.selectedChecker) this.setSelected();
        }
    }

    setSelected() {
        document.getElementById(this.selectedChecker.id).classList.add('choosen');
    }

    rmSelected() {
        document.getElementById(this.selectedChecker.id).classList.remove('choosen');
    }

    secondClick(cell) {
        if (!this.ruler.isPossibleMove(this.game.board, this.game.player, this.selectedChecker, cell)) {
            return;
        }
        this.moveCheckerView(cell);
        let beated = this.selectedChecker.move(this.game.board, cell, this.beatNow);
        if (this.beatNow) {
            this.game.computer.checkers.splice(this.game.computer.checkers.indexOf(beated), 1);
            this.beatCheckerView(beated);
        }
        if (!this.selectedChecker.isKing && this.ruler.isTransformEnabled(this.game, this.selectedChecker)) {
            this.selectedChecker.becomeKing();
            this.becomeKingView(cell.checker);
        }
        if (this.beatNow) {
            let iBNresult = this.ruler.isBeatNeeded(this.game.board, this.game.player);
            let checkerBeats = iBNresult.map(ibn => ibn.checker);
            if (checkerBeats.indexOf(this.selectedChecker) === -1) {
                this.rmSelected();
                this.selectedChecker = false;
                this.beatNow = false;
                this.playerTurn = false;
                this.computerMove();
            }
        } else {
            this.rmSelected();
            this.selectedChecker = false;
            this.beatNow = false;
            this.playerTurn = false;
            this.computerMove();
        }
        if (this.ruler.isGameEnd(this.game)) {
            alert(this.game.getWinner(this.game, this.ruler));
            return;
        }
    }

    computerMove() {
        let possible = this.ruler.getAllMoves(this.game.board, this.game.computer);
        let cell;
        this.beatNow = this.ruler.isBeatNeeded(this.game.board, this.game.computer).length;
        if (!possible.length) {
            return;
        } else {
            // let alive = possible.filter((val) => !isItBeatNext(val.split(" ")));
            // let m = alive.length ? alive[Math.round(Math.random() * alive.length)].split(" ") :
            //     possible[Math.round(Math.random() * possible.length)].split(" ");

            let [checkerPos, nextCellPos] = possible[Math.round(Math.random() * (possible.length - 1))].split(" ");
            this.selectedChecker = this.game.board.cells[this.game.board.letters.indexOf(checkerPos[0])][checkerPos[1]].checker;
            cell = this.game.board.cells[this.game.board.letters.indexOf(nextCellPos[0])][nextCellPos[1]];
        }
        this.moveCheckerView(cell);
        let beated = this.selectedChecker.move(this.game.board, cell, this.beatNow);
        if (this.beatNow) {
            this.game.player.checkers.splice(this.game.player.checkers.indexOf(beated), 1);
            this.beatCheckerView(beated);
        }
        if (!this.selectedChecker.isKing && this.ruler.isTransformEnabled(this.game, this.selectedChecker)) {
            this.selectedChecker.becomeKing();
            this.becomeKingView(cell.checker);
        }
        let checkerBeats;
        if (this.beatNow) {
            let iBNresult = this.ruler.isBeatNeeded(this.game.board, this.game.computer)
            checkerBeats = iBNresult.map(ibn => ibn.checker)
            if (checkerBeats.indexOf(this.selectedChecker) === -1) {
                this.selectedChecker = false;
                this.beatNow = false;
                this.playerTurn = true;
            } else {
                this.computerMove();
            }
        } else {
            this.selectedChecker = false;
            this.beatNow = false;
            this.playerTurn = true;
        }
    }

    moveCheckerView(cell) {
        let prevCell = document.getElementById(this.selectedChecker.cell.pos);
        let futureCell = document.getElementById(cell.pos);
        let checker = document.getElementById(this.selectedChecker.id);
        prevCell.removeChild(checker);
        futureCell.appendChild(checker);
    }

    beatCheckerView(checker) {
        // problem in up-right
        document.getElementById(checker.id).remove();
    }

    becomeKingView(checker) {
        let ceckerDoc = document.getElementById(checker.id);
        if (checker.color === this.game.player.color) {
            ceckerDoc.classList.add('darkKingChecker');
        } else {
            ceckerDoc.classList.add('lightKingChecker');
        }
    }
}
