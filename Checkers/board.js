class Board {
	constructor(n = 8) {
		this.letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
		this.cells = [];
		this.n = n;
	}
	
	fillCells() {
		let arr = [];
		for (let i = 0; i < this.n; i++) {
			arr.push([]);
			for (let j = 1; j <= this.n; j++) {
				let color = (i % 2 === 1 ? (j % 2 === 1 ? 'white' : 'black') : (j % 2 === 1 ? 'black' : 'white'));
				let cell = new Cell(color, this.letters[i] + j);
				arr[i][j] = cell;
			}
		}
		this.cells = arr;
	}
	
	getCellsBetween(cellTo, cellFrom, board = this) {
		let ctCoords = { x: board.letters.indexOf(cellTo.pos[0]), y: Number(cellTo.pos[1]) };
		let cfCoords = { x: board.letters.indexOf(cellFrom.pos[0]), y: Number(cellFrom.pos[1]) };
	
		let xAxis = this.toRange(cfCoords.x, ctCoords.x).slice(1, -1);
		let yAxis = this.toRange(cfCoords.y, ctCoords.y).slice(1, -1);
	
		let axisCells = xAxis.map((x, i) => board.cells[x][yAxis[i]])
		return axisCells.length ? axisCells : [];
	}
	
	range(start, end) {
		return (new Array(end - start + 1)).fill(undefined).map((_, i) => i + start);
	}
	
	toRange(start, end) {
		let r = [...this.range(Math.min(start, end), Math.max(start, end))]
		if (start > end) r = r.reverse();
		return r;
	}
}