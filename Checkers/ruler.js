class Ruler {
	constructor() { }

	isPossibleMove(board, player, checker, cellTo) {
		if (cellTo.checker || cellTo.color === 'white') {
			return;
		}
		let between = board.getCellsBetween(cellTo, checker.cell);
		if (!checker.isKing) {
			if (board.letters.indexOf(checker.cell.pos[0]) <= board.letters.indexOf(cellTo.pos[0])
				&& between.length !== 1) {
				return false;
			}
		}
		if (between.length) {
			if (checker.isKing) {
				return true;
			}
			return (between.length === 1 && between[0].checker
				&& this.isBeatPossible(board, player, checker, between[between.length - 1].checker));
		}
		if (!between.length) {
			return true;
		}
	}

	isBeatPossible(board, player, activeChecker, checkerToBeat) {
		if (!activeChecker.isKing) {
			return activeChecker.id.slice(0, 6) !== checkerToBeat.id.slice(0, 6);
		} else {
			return this.getAllMoves(board, player).indexOf(activeChecker.cell.pos + ' ' + checkerToBeat.cell) !== -1;
		}
	}

	getAllMoves(board, player) {
		let iBNresult = this.isBeatNeeded(board, player);
		if (iBNresult.length > 0) {
			let moves = iBNresult.map(ibn => {
				let cellFrom = ibn.checker.cell.pos;
				let coords = [board.letters.indexOf(cellFrom[0]), Number(cellFrom[1]) - 1];
				return ibn.beated.map(b => {
					let coordTo = coords.map((coord, i) => coord + (b.coords[i] > coord ? 2 : -2));
					return cellFrom + " " + board.cells[coordTo[0]][coordTo[1] + 1].pos;
				})
			}).reduce((a, b) => a.concat(b), []);
			return moves;
		}
		else {
			let bINresult = this.beatIsntNeeded(board, player);
			let moves = bINresult.map(ibn => {
				let cellFrom = ibn.checker.cell.pos;
				let coords = [board.letters.indexOf(cellFrom[0]), Number(cellFrom[1]) - 1];
				return ibn.steps.map(b => cellFrom + " " + b.cell.pos);
			}).reduce((a, b) => a.concat(b), []);
			return moves;
		}
	}

	beatIsntNeeded(board, player) {
		let chekers = player.checkers.reduce((arr, cur) => {
			let coords = [board.letters.indexOf(cur.cell.pos[0]), Number(cur.cell.pos[1]) - 1]
			// [2, 5] => [[1, 3], [4, 6]]
			let possibleSteps = coords.map(c => [c - 1, c + 1])
				// [[1, 3], [4, 6]] => [[1,4], [3,4], [1,6], [3,6]]
				.reduce((a, b) =>
					a.map(x => b.map(y => x.concat(y))).reduce((a, b) => a.concat(b), []
					), [[]])
				// if king or next cell's x is less than now (E < F)
				.filter(c => cur.isKing || (player.direction == (c[0] < coords[0])))
				.filter(c => c.every(p => p > -1 && p < 8))
				.map(c => {
					let cell = board.cells[c[0]][c[1] + 1];
					if (cur.isKing) {
						let cellsForKing = [];
						// counter for how away we are
						let counter = 1;
						// while doesn't have checker - is an option															
						while (!cell.checker) {
							cellsForKing.push({
								coords: coords.map((coord, i) => c[i] + (c[i] > coord ? counter : -counter)),
								cell: cell
							})
							counter += 1;
							let newC = coords.map((coord, i) => c[i] + (c[i] > coord ? counter : -counter))
							// every means both x:1 and y:3 in [1, 3] should be on board
							if (!newC.every(p => p > -1 && p < 8)) break;
							cell = board.cells[newC[0]][newC[1] + 1];
						}
						// return this way, pos and cell
						return cellsForKing;
					}
					// return cell if not king
					else return !cell.checker ? [{ coords: c, cell: cell }] : []
				})
			if (possibleSteps.length > 0) {
				arr.push({ checker: cur, steps: possibleSteps.reduce((a, b) => a.concat(b), []) });
			}
			return arr;
		}, [])
		return chekers;
	}

	// function isItBeatNext(checker, pos) {
	// 	// returns boolean
	// }

	isTransformEnabled(game, checker) {
		if (checker.isKing) {
			return;
		}
		let pos = checker.cell.pos.split('')[0];
		if (checker.color == game.player.color) {
			return game.board.letters[0] === pos;
		} else {
			return game.board.letters[game.board.letters.length - 1] === pos[0];
		}
	}

	isBeatNeeded(board, player) {
		let chekers = player.checkers.reduce((arr, cur) => {
			let coords = [board.letters.indexOf(cur.cell.pos[0]), Number(cur.cell.pos[1]) - 1];
			// [2, 5] => [[1, 3], [4, 6]]
			let possibleBeats = coords.map(c => [c - 1, c + 1])
				.reduce((a, b) =>
					a.map(x => b.map(y => x.concat(y))).reduce((a, b) => a.concat(b), []
					), [[]])
				// filter if out of board
				.filter(c => c.every(p => p > -1 && p < 8))
				.map(c => {
					if (cur.isKing) {
						let cell = board.cells[c[0]][c[1] + 1];
						// if doesn't have checker search for next cell with checker
						while (!cell.checker) {
							c = coords.map((coord, i) => c[i] + (c[i] > coord ? 1 : -1));
							// every means both x:1 and y:3 in [1, 3] should be on board
							if (!c.every(p => p > -1 && p < 8)) break;
							cell = board.cells[c[0]][c[1] + 1];
						}
						// return this way, pos and cell
						// return { coords: c, cell: board.cells[board.letters.indexOf(cell.pos[0])][cell.pos[1] - 1] ? cell : null }
						return { coords: c, cell: cell }
					}
					// return cell if not king
					else {
						let temp = board.cells[c[0]][c[1] + 1];
						return { coords: c, cell: temp.checker ? board.cells[c[0]][c[1] + 1] : null };
					}
				});
			if (possibleBeats.length)
				// filter if there is a checker and if it is not yours
				possibleBeats = possibleBeats.filter(c => c.cell && c.cell.checker && c.cell.checker.id.slice(0, 6) !== cur.id.slice(0, 6))
					.filter(c => {
						// check if next cell is free
						let coordTo = coords.map((coord, i) => coord + (c.coords[i] > coord ? 2 : -2))
						// and within board
						if (!coordTo.every(p => p > -1 && p < 8)) return false
						let cellTo = board.cells[coordTo[0]][coordTo[1] + 1]
						// return true if free
						return !cellTo.checker
					})
			if (possibleBeats.length > 0) arr.push({ checker: cur, beated: possibleBeats });
			return arr;
		}, []);
		return chekers;
	}

	isGameEnd(game) {
		return (this.getAllMoves(game.board, game.player).length === 0 || this.getAllMoves(game.board, game.computer).length === 0)
			|| (!game.player.checkers.length && !game.player.checkers.length);
	}
}