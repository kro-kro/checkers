window.onload = function () {
    let board = new Board();
    board.fillCells();
    sGame = new SimpleGame(board);
    sGame.putCheckers();
    mainGame = new Game(sGame);
    let gameBoard = document.getElementById("gameBoard");
    mainGame.drawBoard(board, gameBoard);
}

let mainGame;

function onBoardClick(event) {
    try {
        if (!mainGame.playerTurn) {
            return;
        }
        let count = mainGame.game.board.n;
        let size = document.getElementById("gameBoard").offsetWidth / count;

        let cell;
        let cellDoc = event.target;
        if (cellDoc.id.length !== 2) {
            cellDoc = cellDoc.parentNode;
        }
        cell = mainGame.game.board.cells[mainGame.game.board.letters.indexOf(cellDoc.id[0])][cellDoc.id[1]];

        if (!mainGame.selectedChecker) {
            mainGame.firstClick(cell);
        } else {
            mainGame.secondClick(cell);
        }
    } catch (error) { }
}