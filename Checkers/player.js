class Player {
	constructor(color, direction = true) {
		this.color = color;
		this.checkers = [];
		this.direction = direction;
	}
}